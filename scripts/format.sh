#!/usr/bin/env sh
clang-format -i src/*.c
clang-format -i src/*.h
clang-format -i shaders/*.vert
clang-format -i shaders/*.frag
