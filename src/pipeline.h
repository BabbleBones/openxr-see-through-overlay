/*
 * OpenXR Video Seethrough Overlay
 *
 * Copyright 2017-2021 Collabora Ltd.
 *
 * Authors: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#pragma once

#include "vulkan_framebuffer.h"

typedef struct
{
  VkDevice device;
  VkPipeline pipeline;
  VkPipelineLayout pipeline_layout;

  VkDescriptorPool descriptor_pool;
  VkDescriptorSetLayout descriptor_set_layout;

  VkDescriptorSet descriptor_sets[2];

  VkSampler sampler;

  struct
  {
    vulkan_buffer views[2];
  } uniform_buffers;

  struct UBOView
  {
    int view_index;
  } ubo_views[2];
} pipeline;

void
pipeline_init(pipeline* self,
              vulkan_device* vulkan_device,
              VkRenderPass render_pass,
              VkPipelineCache pipeline_cache);

void
pipeline_destroy(pipeline* self);

void
pipeline_draw(pipeline* self, VkCommandBuffer cmd_buffer, uint32_t eye);

void
pipeline_update_image_view(pipeline* self,
                           uint32_t eye,
                           VkImageView image_view);
