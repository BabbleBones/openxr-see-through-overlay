/*
 * OpenXR Video Seethrough Overlay
 *
 * Copyright 2020-2021 Collabora Ltd.
 *
 * Authors: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "log.h"
#include <gio/gio.h>

static gboolean
_load_resource(const gchar *path, GBytes **res)
{
  GError *error = NULL;
  *res = g_resources_lookup_data(path, G_RESOURCE_LOOKUP_FLAGS_NONE, &error);

  if (error != NULL) {
    g_printerr("Unable to read file: %s\n", error->message);
    g_error_free(error);
    return FALSE;
  }

  return TRUE;
}

bool
vulkan_shader_create_module(VkDevice device,
                            const char *resource_name,
                            VkShaderModule *module)
{
  GBytes *bytes;
  if (!_load_resource(resource_name, &bytes))
    return false;

  gsize size = 0;
  const uint32_t *buffer = g_bytes_get_data(bytes, &size);

  VkShaderModuleCreateInfo info = {
    .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
    .codeSize = size,
    .pCode = buffer,
  };

  VkResult res = vkCreateShaderModule(device, &info, NULL, module);
  if (res != VK_SUCCESS) {
    log_e("Could not create shader module for %s.", resource_name);
    return false;
  }

  g_bytes_unref(bytes);

  return true;
}
