/*
 * OpenXR Video Seethrough Overlay
 *
 * Copyright 2017-2021 Collabora Ltd.
 *
 * Authors: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "pipeline.h"

#include "log.h"
#include "vulkan_shader.h"

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

static void
_create_sampler(VkDevice device, VkSampler* sampler)
{
  VkSamplerCreateInfo info = {
    .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
    .magFilter = VK_FILTER_LINEAR,
    .minFilter = VK_FILTER_LINEAR,
    .mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR,
    .addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT,
    .addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT,
    .addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT,
    .mipLodBias = 0.0f,
    .anisotropyEnable = VK_TRUE,
    .maxAnisotropy = 8,
    .compareOp = VK_COMPARE_OP_NEVER,
    .borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE,
  };

  vk_check(vkCreateSampler(device, &info, NULL, sampler));
}

static void
_init_descriptor_pool(pipeline* self)
{
  VkDescriptorPoolSize poolSizes[2] = {
    { .type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, .descriptorCount = 3 },
    { .type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, .descriptorCount = 3 }
  };

  VkDescriptorPoolCreateInfo descriptorPoolInfo = {
    .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
    .maxSets = 2,
    .poolSizeCount = ARRAY_SIZE(poolSizes),
    .pPoolSizes = poolSizes
  };

  vk_check(vkCreateDescriptorPool(self->device, &descriptorPoolInfo, NULL,
                                  &self->descriptor_pool));
}

static void
_init_descriptor_set_layouts(pipeline* self)
{
  VkDescriptorSetLayoutBinding setLayoutBindings[2] = {
    // Binding 0 : Fragment shader ubo
    { .binding = 0,
      .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
      .descriptorCount = 1,
      .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT },
    // Binding 1 : texture
    { .binding = 1,
      .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
      .descriptorCount = 1,
      .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT },
  };

  VkDescriptorSetLayoutCreateInfo descriptorLayout = {
    .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
    .bindingCount = ARRAY_SIZE(setLayoutBindings),
    .pBindings = setLayoutBindings
  };
  vk_check(vkCreateDescriptorSetLayout(self->device, &descriptorLayout, NULL,
                                       &self->descriptor_set_layout));

  VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo = {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
    .setLayoutCount = 1,
    .pSetLayouts = &self->descriptor_set_layout
  };
  vk_check(vkCreatePipelineLayout(self->device, &pipelineLayoutCreateInfo, NULL,
                                  &self->pipeline_layout));
}

static void
_init_descriptor_sets(pipeline* self, uint32_t eye)
{
  VkDescriptorSetAllocateInfo allocInfo = {
    .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
    .descriptorPool = self->descriptor_pool,
    .descriptorSetCount = 1,
    .pSetLayouts = &self->descriptor_set_layout
  };
  vk_check(vkAllocateDescriptorSets(self->device, &allocInfo,
                                    &self->descriptor_sets[eye]));
}


static void
_update_ubo(pipeline* self, uint32_t eye)
{
  VkWriteDescriptorSet writeDescriptorSets[1] = {
    // Binding 0 : Vertex shader ubo
    (VkWriteDescriptorSet){
      .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
      .dstSet = self->descriptor_sets[eye],
      .dstBinding = 0,
      .descriptorCount = 1,
      .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
      .pBufferInfo = &self->uniform_buffers.views[eye].descriptor,
    },
  };

  vkUpdateDescriptorSets(self->device, ARRAY_SIZE(writeDescriptorSets),
                         writeDescriptorSets, 0, NULL);

  self->ubo_views[eye].view_index = eye;
  memcpy(self->uniform_buffers.views[eye].mapped, &self->ubo_views[eye],
         sizeof(self->ubo_views[eye]));
}

static void
_init_pipeline(pipeline* self,
               VkRenderPass render_pass,
               VkPipelineCache pipeline_cache)
{
  VkPipelineInputAssemblyStateCreateInfo inputAssemblyState = {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
    .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
    .primitiveRestartEnable = VK_FALSE
  };

  VkPipelineRasterizationStateCreateInfo rasterizationState = {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
    .depthClampEnable = VK_FALSE,
    .polygonMode = VK_POLYGON_MODE_FILL,
    .cullMode = VK_CULL_MODE_BACK_BIT,
    .frontFace = VK_FRONT_FACE_CLOCKWISE,
    .lineWidth = 1.0f
  };

  VkPipelineColorBlendAttachmentState blendAttachmentState = {
    .blendEnable = VK_FALSE, .colorWriteMask = 0xf
  };

  VkPipelineColorBlendStateCreateInfo colorBlendState = {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
    .attachmentCount = 1,
    .pAttachments = &blendAttachmentState
  };

  VkPipelineDepthStencilStateCreateInfo depthStencilState = {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
    .depthTestEnable = VK_TRUE,
    .depthWriteEnable = VK_FALSE,
    .depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL,
    .front = { .compareOp = VK_COMPARE_OP_ALWAYS },
    .back = { .compareOp = VK_COMPARE_OP_ALWAYS }
  };

  VkPipelineViewportStateCreateInfo viewportState = {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
    .viewportCount = 1,
    .scissorCount = 1
  };

  VkPipelineMultisampleStateCreateInfo multisampleState = {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
    .rasterizationSamples = VK_SAMPLE_COUNT_1_BIT
  };

  VkDynamicState dynamicStateEnables[2] = {
    VK_DYNAMIC_STATE_VIEWPORT,
    VK_DYNAMIC_STATE_SCISSOR,
  };

  VkPipelineDynamicStateCreateInfo dynamicState = {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
    .dynamicStateCount = ARRAY_SIZE(dynamicStateEnables),
    .pDynamicStates = dynamicStateEnables
  };

  VkPipelineVertexInputStateCreateInfo vertexInputState = {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
  };

  VkShaderModule frag_shader;
  if (!vulkan_shader_create_module(self->device, "/shaders/seethrough.frag.spv",
                                   &frag_shader))
    return;

  VkShaderModule vert_shader;
  if (!vulkan_shader_create_module(self->device, "/shaders/seethrough.vert.spv",
                                   &vert_shader))
    return;

  VkPipelineShaderStageCreateInfo shaderStages[2] = {
    { .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
      .stage = VK_SHADER_STAGE_VERTEX_BIT,
      .module = vert_shader,
      .pName = "main" },
    { .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
      .stage = VK_SHADER_STAGE_FRAGMENT_BIT,
      .module = frag_shader,
      .pName = "main" },
  };

  VkGraphicsPipelineCreateInfo pipelineCreateInfo = {
    .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
    .stageCount = ARRAY_SIZE(shaderStages),
    .pStages = shaderStages,
    .pVertexInputState = &vertexInputState,
    .pInputAssemblyState = &inputAssemblyState,
    .pViewportState = &viewportState,
    .pRasterizationState = &rasterizationState,
    .pMultisampleState = &multisampleState,
    .pDepthStencilState = &depthStencilState,
    .pColorBlendState = &colorBlendState,
    .pDynamicState = &dynamicState,
    .layout = self->pipeline_layout,
    .renderPass = render_pass,
    .basePipelineHandle = VK_NULL_HANDLE,
    .basePipelineIndex = -1
  };

  vk_check(vkCreateGraphicsPipelines(self->device, pipeline_cache, 1,
                                     &pipelineCreateInfo, NULL,
                                     &self->pipeline));

  vkDestroyShaderModule(self->device, frag_shader, NULL);
  vkDestroyShaderModule(self->device, vert_shader, NULL);
}

static void
_init_uniform_buffers(pipeline* self, vulkan_device* vk_device)
{
  for (uint32_t i = 0; i < 2; i++)
    vulkan_device_create_and_map(vk_device, &self->uniform_buffers.views[i],
                                 sizeof(self->ubo_views[i]));
}

void
pipeline_init(pipeline* self,
              vulkan_device* vk_device,
              VkRenderPass render_pass,
              VkPipelineCache pipeline_cache)
{
  self->device = vk_device->device;
  _create_sampler(self->device, &self->sampler);
  _init_uniform_buffers(self, vk_device);
  _init_descriptor_set_layouts(self);
  _init_pipeline(self, render_pass, pipeline_cache);
  _init_descriptor_pool(self);
  for (uint32_t i = 0; i < 2; i++) {
    _init_descriptor_sets(self, i);
    _update_ubo(self, i);
  }
}

void
pipeline_destroy(pipeline* self)
{
  vkDestroyPipeline(self->device, self->pipeline, NULL);
  vkDestroyPipelineLayout(self->device, self->pipeline_layout, NULL);
  vkDestroyDescriptorSetLayout(self->device, self->descriptor_set_layout, NULL);
  vkDestroyDescriptorPool(self->device, self->descriptor_pool, NULL);
  for (uint32_t i = 0; i < 2; i++)
    vulkan_buffer_destroy(&self->uniform_buffers.views[i]);
}

void
pipeline_draw(pipeline* self, VkCommandBuffer cmd_buffer, uint32_t eye)
{
  vkCmdBindPipeline(cmd_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS,
                    self->pipeline);
  vkCmdBindDescriptorSets(cmd_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS,
                          self->pipeline_layout, 0, 1,
                          &self->descriptor_sets[eye], 0, NULL);

  /* Draw 3 verts from which we construct the fullscreen quad in
   * the shader*/
  vkCmdDraw(cmd_buffer, 3, 1, 0, 0);
}

void
pipeline_update_image_view(pipeline* self, uint32_t eye, VkImageView image_view)
{
  VkDescriptorImageInfo descriptor = (VkDescriptorImageInfo){
    .sampler = self->sampler,
    .imageView = image_view,
    .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
  };

  VkWriteDescriptorSet writeDescriptorSets[1] = {
    // Binding 1 : Fragment shader color map
    (VkWriteDescriptorSet){
      .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
      .dstSet = self->descriptor_sets[eye],
      .dstBinding = 1,
      .descriptorCount = 1,
      .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
      .pImageInfo = &descriptor,
    },
  };

  vkUpdateDescriptorSets(self->device, ARRAY_SIZE(writeDescriptorSets),
                         writeDescriptorSets, 0, NULL);
}
