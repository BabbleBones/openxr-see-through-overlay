/*
 * OpenXR Video Seethrough Overlay
 *
 * Copyright 2017-2020 Collabora Ltd.
 *
 * Authors: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "settings.h"

#include <stdint.h>
#include <getopt.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include "log.h"

static void
_init(settings *self)
{
  self->device_path = NULL;
}

static const char *
_help_string()
{
  return "A Vulkan OpenXR video seethrogh overlay\n"
         "\n"
         "Options:\n"
         "  -d, --device VIDEO_DEVICE    Video device path (default: "
         "/dev/video0)\n"
         "  -h, --help                   Show this help\n";
}

bool
settings_parse_args(settings *self, int argc, char *argv[])
{
  _init(self);
  int option_index = -1;
  static const char *optstring = "hd:";

  struct option long_options[] = { { "help", 0, 0, 0 },
                                   { "device", 0, 0, 0 },
                                   { 0, 0, 0, 0 } };

  const char *optname = NULL;

  int opt;
  while ((opt = getopt_long(argc, argv, optstring, long_options,
                            &option_index)) != -1) {
    if (opt == '?' || opt == ':')
      return false;

    if (option_index != -1)
      optname = long_options[option_index].name;

    if (opt == 'h' || (optname && strcmp(optname, "help") == 0)) {
      printf("%s\n", _help_string());
      exit(0);
    } else if (opt == 'd' || (optname && strcmp(optname, "device") == 0)) {
      self->device_path = malloc(strlen(optarg));
      strcpy(self->device_path, optarg);
    } else {
      log_f("Unknown option %s", optname);
    }
  }

  if (optind != argc)
    log_w("trailing args");

  return true;
}
