/*
 * OpenXR Video Seethrough Overlay
 *
 * Copyright 2020 Collabora Ltd.
 *
 * Authors: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#pragma once

#include <vulkan/vulkan.h>
#include <stdbool.h>

bool
vulkan_shader_create_module(VkDevice device,
                            const char* resource_name,
                            VkShaderModule* module);
